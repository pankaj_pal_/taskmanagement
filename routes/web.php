<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::post('/add-project', [App\Http\Controllers\ProjectController::class, 'add'])->name('add-project');
Route::get('/task', [App\Http\Controllers\TaskController::class, 'index'])->name('task');
Route::get('/manage-task', [App\Http\Controllers\TaskController::class, 'manageTask'])->name('manage-task');
Route::get('/edit/{id}', [App\Http\Controllers\TaskController::class, 'editTask']);
Route::get('/delete/{id}', [App\Http\Controllers\TaskController::class, 'deleteTask']);
Route::get('/detail/{id}', [App\Http\Controllers\TaskController::class, 'detail']);
Route::get('/reply/{id}', [App\Http\Controllers\TaskController::class, 'reply']);
Route::post('/add-task', [App\Http\Controllers\TaskController::class, 'add'])->name('save-task');
Route::post('/update-task', [App\Http\Controllers\TaskController::class, 'update']);
Route::post('/add-comment', [App\Http\Controllers\TaskController::class, 'addComment']);
Route::post('/reply-comment', [App\Http\Controllers\TaskController::class, 'replyComment']);
Route::get('/manage-products', [App\Http\Controllers\ProductController::class, 'index'])->name('product');
Route::get('/add-product', [App\Http\Controllers\ProductController::class, 'add'])->name('add-product');
Route::post('/add-product', [App\Http\Controllers\ProductController::class, 'add'])->name('save-product');	
Route::get('/edit/{id}', [App\Http\Controllers\ProductController::class, 'editProduct']);
Route::post('/update-product', [App\Http\Controllers\ProductController::class, 'updateProduct']);
Route::get('/sms-categorization', [App\Http\Controllers\CategoryController::class, 'index'])->name('category');
Route::get('/add-category', [App\Http\Controllers\CategoryController::class, 'add'])->name('add-category');
Route::post('/add-category', [App\Http\Controllers\CategoryController::class, 'add'])->name('save-category');
Route::get('/edit/{id}', [App\Http\Controllers\CategoryController::class, 'editCategory']);
Route::post('/update-category', [App\Http\Controllers\CategoryController::class, 'updateCategory']);
Route::get('/credit-points', [App\Http\Controllers\PointController::class, 'index'])->name('point');
Route::get('/add-point', [App\Http\Controllers\PointController::class, 'add'])->name('add-point');
Route::post('/add-point', [App\Http\Controllers\PointController::class, 'add'])->name('save-point');
Route::get('/edit/{id}', [App\Http\Controllers\PointController::class, 'editPoint']);
Route::post('/update-point', [App\Http\Controllers\PointController::class, 'updatePoint']);


