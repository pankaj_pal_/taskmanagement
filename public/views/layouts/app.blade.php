<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

      <title>{{ config('app.name', 'Login') }}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- App favicon -->
       <link  href="{{ asset('images/favicon.ico') }}" rel="stylesheet">
       <link  href="{{ asset('css/preloader.min.css') }}" rel="stylesheet">
       <link  href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
       <link  href="{{ asset('css/icons.min.css') }}" rel="stylesheet">
       <link  href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
       <link  href="{{ asset('css/icons.min.css') }}" rel="stylesheet">
       <link  href="{{ asset('css/app.min.css') }}" rel="stylesheet">
        <!-- preloader css -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        <!-- Bootstrap Css -->
         <link id="bootstrap-style"  href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
       
        <!-- Icons Css -->
        <link type="text/css"  href="{{ asset('css/icons.min.css') }}" rel="stylesheet">
        
        <!-- App Css-->
        <link type="text/css"  href="{{ asset('css/app.min.css') }}" rel="stylesheet">
    </head>

    <body>
        <ul class="list-group">
                                <!-- Authentication Links -->
                        @guest
                            @if (Route::has('login'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                            @endif

                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item">
                                
                                    {{ Auth::user()->name }}
                                
                                <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('add-project') }}">Add Project</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('task') }}">Add Task</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('manage-task') }}">Manage Task</a>
                                    </li>
                                    

                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                        </ul> 
    <!-- <body data-layout="horizontal"> -->
        @yield('content')

        <!-- JAVASCRIPT -->
        <script src="{{ asset('libs/jquery/jquery.min.js') }}" defer></script>
        <script src="{{ asset('bootstrap/js/bootstrap.bundle.min.js') }}" defer></script>
        <script src="{{ asset('libs/metismenu/metisMenu.min.js') }}" defer></script>
        <script src="{{ asset('libs/simplebar/simplebar.min.js') }}" defer></script>
        <script src="{{ asset('libs/node-waves/waves.min.js') }}" defer></script>
        <script src="{{ asset('libs/feather-icons/feather.min.js') }}" defer></script>
        
        <!-- pace js -->
        <script src="{{ asset('libs/pace-js/pace.min.js') }}" defer></script>

        <!-- password addon init -->
        <script src="{{ asset('js/pages/pass-addon.init.js') }}" defer></script>


    </body>


</html>

<style type="text/css">
    .list-group li{
        list-style: none;
    }
</style>






