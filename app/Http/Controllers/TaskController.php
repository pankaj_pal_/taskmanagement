<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Log;
use App\Models\Project;
use App\Models\Task;
use App\Models\Comment;
use App\Models\ReplyComment;
use Illuminate\Support\Facades\Auth;


class TaskController extends Controller
{
	/**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /*
      form to add task
    */
    public function index()
    {
    	    $userId = Auth::check() ? Auth::id() : true;
    	    $projectInfo = Project::select('id','name')->where('user_id',$userId)->get()->toArray();
    	    return view('task/add',['projectInfo'=>$projectInfo]);
    }

	/*
      form to manage task
    */
	public function manageTask()
    {
    		$userId = Auth::check() ? Auth::id() : true;
    	    $taskList = Task::where('user_id',$userId)
                    ->get();
           return view('task/list',['taskList'=>$taskList]);
    }

    /*
      form to manage task
    */
	public function editTask($id)
    {

    	  $userId = Auth::check() ? Auth::id() : true;
    	  $projectInfo = Project::select('id','name')->where('user_id',$userId)->get()->toArray();
		  $taskList = Task::where('id',$id)->first();
          return view('task/edit',['taskList'=>$taskList,'projectInfo'=>$projectInfo]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request)
    {
        Log::info('Add Comment:add');
        $userId = Auth::check() ? Auth::id() : true;
		$rules = [
            'name' => 'required|string|min:3|max:255',
            'project_id'=>'required|integer',
            'status'=>'required|integer'
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return redirect('insert')
            ->withInput()
            ->withErrors($validator);
        }
        else{
        $data = $request->post();
        try{
            $task = new Task;
            $task->user_id = $userId;
            $task->project_id = $data['project_id']; 
            $task->name = $data['name'];
            $task->start_date =  date('Y-m-d h:i',strtotime($data['start_date']));
			$task->end_date =  date('Y-m-d h:i',strtotime($data['end_date']));
            $task->name = $data['name'];
			$task->description = !empty($data['description'])?$data['description']:'test description';	
            $task->save();
            return redirect('manage-task')->with('status',"Insert successfully");
        }
        catch(Exception $ex){
                     Log::info('Something went wrong while calling  method: ' . $ex->getMessage());
                     return redirect('add-project')->with('failed',"operation failed");
        }

    }
        
        //save project information
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $userId = Auth::check() ? Auth::id() : true;
		$rules = [
            'name' => 'required|string|min:3|max:255',
            'project_id'=>'required|integer',
            'status'=>'required|integer'
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return redirect('insert')
            ->withInput()
            ->withErrors($validator);
        }
        else{
        $data = $request->post();
		try{
            $taskList = Task::find($data['task_id']);
		    $taskList->user_id = $userId;
			$taskList->project_id = $data['project_id']; 
            $taskList->name = $data['name'];
            $taskList->start_date =  date('Y-m-d h:i',strtotime($data['start_date']));
			$taskList->end_date =  date('Y-m-d h:i',strtotime($data['end_date']));
            $taskList->name = $data['name'];
            if(!empty($data['description'])){
            		$taskList->description = $data['description'];	
            }
			$taskList->save();
            return redirect('manage-task')->with('status',"Updated  successfully");
        }
        catch(Exception $ex){
                     Log::info('Something went wrong while calling  method: ' . $ex->getMessage());
                     return redirect('manage-task')->with('failed',"operation failed");
        }

    }
		//save project information
    }

    	/*
    	 function to delete task
    	*/
    public function deleteTask($id){
    	 $taskList = Task::find($id);
    	 $taskList->delete();
    	return redirect('manage-task')->with('status',"deleted Task successfully");	
    }	

    /*
    show task details
    */
    	/*
      form to manage task
    */
	public function detail($id)
    {
    		$userId = Auth::check() ? Auth::id() : true;
    	    $taskList = Task::find($id);
           return view('task/detail',['taskList'=>$taskList]);
    }

    /*
     add comment on task
    */
     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addComment(Request $request)
    {
        $userId = Auth::check() ? Auth::id() : true;

        $rules = [
            'description' => 'required|string|min:3|max:255'
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return redirect('manage-task')
            ->withInput()
            ->withErrors($validator);
        }
        else{
        $data = $request->post();
        
        try{

            $comment = new Comment;
            $comment->user_id = $userId;
            $comment->project_id = $data['project_id']; 
            $comment->description = $data['description'];
            $comment->task_id = $data['task_id'];
            $comment->save();
        	return redirect('detail/'.$data['task_id'])->with('status',"Comment Added");
        }
        catch(Exception $ex){
                     Log::info('Something went wrong while calling  method: ' . $ex->getMessage());
                     return redirect('manage-task')->with('failed',"operation failed");
        }

    }
	}
	    /*
    
       to reply  comment
    */
	public function reply($id)
    {
    		$userId = Auth::check() ? Auth::id() : true;
    	    $commentList = Comment::find($id);

           return view('task/reply',['commentList'=>$commentList]);
    }

     /*
     save comment on reply
    */
     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function replyComment(Request $request)
    {
        $userId = Auth::check() ? Auth::id() : true;

        $rules = [
            'description' => 'required|string|min:3|max:255'
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return redirect('manage-task')
            ->withInput()
            ->withErrors($validator);
        }
        else{
        $data = $request->post();

        try{

            $replyComment = new ReplyComment;
            $replyComment->user_id = $userId;
            $replyComment->project_id = $data['project_id'];
            $replyComment->task_id = $data['task_id'];
            $replyComment->comment_id = $data['comment_id'];
            $replyComment->description = $data['description'];
            $replyComment->status = 1;
            $replyComment->save();
            
        	return redirect('/detail/'.$data['task_id'])->with('status',"Comment Added");
        }
        catch(Exception $ex){
                     Log::info('Something went wrong while calling  method: ' . $ex->getMessage());
                     return redirect('manage-task')->with('failed',"operation failed");
        }

    }
	}

}
