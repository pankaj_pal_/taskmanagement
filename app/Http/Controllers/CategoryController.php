<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Facades\Auth;
class CategoryController extends Controller
{
	
  public function index()
    {
		$userId = Auth::check() ? Auth::id() : true;
		$categoryInfo = Category::where('user_id',$userId)->get()->toArray();
		return view('category/index',['categoryInfo'=>$categoryInfo]);
        
    }
    public function add(Request $request)
    {
		$data = $request->post();
		$userId = Auth::check() ? Auth::id() : true;
		$category = new Category;
		$category->user_id = $userId;
		if(!empty($data['category'])){
			$category->category = $data['category'];
		}
		if(!empty($data['keywords'])){
			$category->keywords = $data['keywords'];
		}
		if(!empty($data)) {
			$category->save();
			return redirect('sms-categorization')->with('status',"Insert successfully");
		}
		
		//$category->category_id = !empty($data['category_id'])?$data['category_id']:0;
		return view('category/add');
        
    }
	 public function editCategory(Request $request,$id)
    {
		
		$data = $request->post();
		$userId = Auth::check() ? Auth::id() : true;
		$categoryInfo = Category::find($id);
		
		
		if(!empty($data['category'])){
			$categoryInfo->category = $data['category'];
		}
		if(!empty($data['keywords'])){
			$categoryInfo->keywords = $data['keywords'];
		}
		
		
		if(!empty($data)){
			
			
			
				$categoryInfo->save();
				return redirect('sms-categorization')->with('status',"Insert successfully");
				
		}
		return view('category/edit',['categoryInfo'=>$categoryInfo]);
        //
    }
	
	public function updateCategory(Request $request)
    {
		
		$data = $request->post();
		$userId = Auth::check() ? Auth::id() : true;
		
		$categoryInfo = Category::find($data['category_id']);
		
		
		if(!empty($data['category'])){
			$categoryInfo->category = $data['category'];
		}
		if(!empty($data['keywords'])){
			$categoryInfo->keywords = $data['keywords'];
		}
		if(!empty($data)){
				$categoryInfo->save();
				return redirect('sms-categorization')->with('status',"Insert successfully");
				
		}
        //
    }
    //
}
