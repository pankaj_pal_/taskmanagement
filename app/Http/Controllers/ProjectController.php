<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Log;
use App\Models\Project;
use App\Models\Task;
use Illuminate\Support\Facades\Auth;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function view()
    {
               
        return view('project/add');
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request)
    {
        $userId = Auth::check() ? Auth::id() : true;

        $rules = [
            'name' => 'required|string|min:3|max:255',
            
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return redirect('insert')
            ->withInput()
            ->withErrors($validator);
        }
        else{
        $data = $request->post();
        try{
            $project = new Project;
            $project->user_id = $userId;
            $project->name = $data['name'];
            $project->description = $data['description'];
            $project->save();
            return redirect('home')->with('status',"Insert successfully");
        }
            catch(Exception $ex){
                     Log::info('Something went wrong while calling  method: ' . $ex->getMessage());
                     return redirect('add-project')->with('failed',"operation failed");
            }

    }
        
        //save project information
    }

   }
