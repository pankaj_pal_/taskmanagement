<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Product;
use App\Models\Charge;
class ProductController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
		$userId = Auth::check() ? Auth::id() : true;
		$productInfo = Product::where('user_id',$userId)->get()->toArray();
		return view('product/index',['productInfo'=>$productInfo]);
        
    }
    public function add(Request $request)
    {
		$data = $request->post();
		$userId = Auth::check() ? Auth::id() : true;
		$product = new Product;
		$product->user_id = $userId;
		$product->category_id = !empty($data['category_id'])?$data['category_id']:0;
		if(!empty($data['product_name'])){
			$product->product_name = $data['product_name'];
		}		
		if(!empty($data['loan_amount_min'])){
			$product->loan_amount_min = $data['loan_amount_min'];
		}
		if(!empty($data['loan_amount_max'])){
			$product->loan_amount_max = $data['loan_amount_max'];
		}
		if(!empty($data['tenure'])){
			$product->tenure = $data['tenure'];
		}
		
		if(!empty($data['tenure_type'])){
			
			$product->tenure_type = $data['tenure_type'];
		}
		if(!empty($data['rate_of_interest'])){
			$product->rate_of_interest = $data['rate_of_interest'];
		}
		if(!empty($data['frequency_id'])){
			$product->frequency_id = $data['frequency_id'];
		}
		if(!empty($data['add_charges'])){
			$product->add_charges = $data['add_charges'];
		}
		
		if(!empty($data['amount'])){
			$product->amount = $data['amount'];
		}
		if(!empty($data['gst_applicable'])){
			$product->gst_applicable = $data['gst_applicable'];
		}
		if(!empty($data['gst_percentage_amount'])){
			$product->gst_percentage_amount = $data['gst_percentage_amount'];
		}
		if(!empty($data['penal_amount'])){
			$product->penal_amount = $data['penal_amount'];
		}
		if(!empty($data['penal_amount'])){
			$product->penal_amount = $data['penal_amount'];
		}
		if(!empty($data['penal_intrest'])){
			$product->penal_intrest = $data['penal_intrest'];
		}
		if(!empty($data->check_ecs_charges)){
			$product->check_ecs_charges = $data['check_ecs_charges'];
		}
		if(!empty($data['gst_percentage'])){
			$product->gst_percentage = $data['gst_percentage'];
		}
		if(!empty($data['approve_amount'])){
			$product->approve_amount = $data['approve_amount'];
		}
		if(!empty($data['processing_fee'])){
			$product->processing_fee = $data['processing_fee'];
		}
		
		if(!empty($data['gst_applicable'])){
			$product->gst_applicable = $data['gst_applicable'];
		}
		
		if(!empty($data)){
			
			
				$product->save();
					
					
				if(!empty($data['group-a'])){
					foreach($data['group-a'] as $key=>$value){
						$charges  = new Charge;
						$charges->charge_name = !empty($value['charge_name'])?$value['charge_name']:null;
						$charges->charge_type = !empty($value['charge_type'])?$value['charge_type']:null;
						$charges->charge_distrubal_amount = !empty($value['charge_distrubal_amount'])?$value['charge_distrubal_amount']:null;
						$charges->charge_percentage = !empty($value['charge_percentage'])?$value['charge_percentage']:null;
						$charges->user_id =  $userId;
						$charges->product_id   =  $product->id;
						$charges->save();
					}	
				}
				return redirect('manage-products')->with('status',"Insert successfully");
				
		}
		
		//echo $product->id;die;
		return view('product/add');
        //
    }
	 public function editProduct(Request $request,$id)
    {
		
		$data = $request->post();
		$userId = Auth::check() ? Auth::id() : true;
		$product = new Product;
		$product->user_id = $userId;
		$product->category_id = !empty($data['category_id'])?$data['category_id']:0;
		$productInfo = Product::find($id);
		
		
		if(!empty($data['product_name'])){
			$product->product_name = $data['product_name'];
		}
		if(!empty($data['loan_amount_min'])){
			$product->loan_amount_min = $data['loan_amount_min'];
		}
		if(!empty($data['loan_amount_max'])){
			$product->loan_amount_max = $data['loan_amount_max'];
		}
		if(!empty($data['tenure'])){
			$product->tenure = $data['tenure'];
		}
		if(!empty($data['tenure_type'])){
			$product->tenure_type = $data['tenure_type'];
		}
		if(!empty($data['rate_of_interest'])){
			$product->rate_of_interest = $data['rate_of_interest'];
		}
		if(!empty($data['frequency_id'])){
			$product->frequency_id = $data['frequency_id'];
		}
		if(!empty($data['add_charges'])){
			$product->add_charges = $data['add_charges'];
		}
		if(!empty($data['amount'])){
			$product->amount = $data['amount'];
		}
		if(!empty($data['gst_applicable'])){
			$product->gst_applicable = $data['gst_applicable'];
		}
		if(!empty($data['gst_percentage_amount'])){
			$product->gst_percentage_amount = $data['gst_percentage_amount'];
		}
		if(!empty($data['penal_amount'])){
			$product->penal_amount = $data['penal_amount'];
		}
		if(!empty($data['penal_amount'])){
			$product->penal_amount = $data['penal_amount'];
		}
		if(!empty($data['penal_intrest'])){
			$product->penal_intrest = $data['penal_intrest'];
		}
		if(!empty($data->check_ecs_charges)){
			$product->check_ecs_charges = $data['check_ecs_charges'];
		}
		if(!empty($data['gst_percentage'])){
			$product->gst_percentage = $data['gst_percentage'];
		}
		if(!empty($data['approve_amount'])){
			$product->approve_amount = $data['approve_amount'];
		}
		if(!empty($data['processing_fee'])){
			$product->processing_fee = $data['processing_fee'];
		}
		
		if(!empty($data['gst_applicable'])){
			$product->gst_applicable = $data['gst_applicable'];
		}
		if(!empty($data['check_ecs_charges'])){
			$product->check_ecs_charges = $data['check_ecs_charges'];
		}
		
		if(!empty($data)){
			
			
			
				$product->save();
					
				if(!empty($data['group-a'])){
					foreach($data['group-a'] as $key=>$value){
						$charges  = new Charge;
						$charges->charge_name = !empty($value['charge_name'])?$value['charge_name']:null;
						$charges->charge_type = !empty($value['charge_type'])?$value['charge_type']:null;
						$charges->charge_distrubal_amount = !empty($value['charge_distrubal_amount'])?$value['charge_distrubal_amount']:null;
						$charges->charge_percentage = !empty($value['charge_percentage'])?$value['charge_percentage']:null;
						$charges->user_id =  $userId;
						$charges->product_id   =  $product->id;
						$charges->save();
					}	
				}
				return redirect('manage-products')->with('status',"Insert successfully");
				
		}
		return view('product/edit',['productInfo'=>$productInfo]);
        //
    }
	
	public function updateProduct(Request $request)
    {
		
		$data = $request->post();
		
		
		$userId = Auth::check() ? Auth::id() : true;
		$product =  Product::find($data['product_id']);
		$product->user_id = $userId;
		$product->category_id = !empty($data['category_id'])?$data['category_id']:0;
		if(!empty($data['product_name'])){
			$product->product_name = $data['product_name'];
		}
		if(!empty($data['loan_amount_min'])){
			$product->loan_amount_min = $data['loan_amount_min'];
		}
		if(!empty($data['loan_amount_max'])){
			$product->loan_amount_max = $data['loan_amount_max'];
		}
		if(!empty($data['tenure'])){
			$product->tenure = $data['tenure'];
		}
		if(!empty($data['tenure_type'])){
			$product->tenure_type = $data['tenure_type'];
		}
		if(!empty($data['rate_of_interest'])){
			$product->rate_of_interest = $data['rate_of_interest'];
		}
		if(!empty($data['frequency_id'])){
			$product->frequency_id = $data['frequency_id'];
		}
		if(!empty($data['add_charges'])){
			$product->add_charges = $data['add_charges'];
		}
		if(!empty($data['amount'])){
			$product->amount = $data['amount'];
		}
		if(!empty($data['gst_applicable'])){
			$product->gst_applicable = $data['gst_applicable'];
		}
		if(!empty($data['gst_percentage_amount'])){
			$product->gst_percentage_amount = $data['gst_percentage_amount'];
		}
		if(!empty($data['penal_amount'])){
			$product->penal_amount = $data['penal_amount'];
		}
		if(!empty($data['penal_amount'])){
			$product->penal_amount = $data['penal_amount'];
		}
		if(!empty($data['penal_intrest'])){
			$product->penal_intrest = $data['penal_intrest'];
		}
		if(!empty($data->check_ecs_charges)){
			$product->check_ecs_charges = $data['check_ecs_charges'];
		}
		if(!empty($data['gst_percentage'])){
			$product->gst_percentage = $data['gst_percentage'];
		}
		if(!empty($data['approve_amount'])){
			$product->approve_amount = $data['approve_amount'];
		}
		if(!empty($data['processing_fee'])){
			$product->processing_fee = $data['processing_fee'];
		}
		
		if(!empty($data['gst_applicable'])){
			$product->gst_applicable = $data['gst_applicable'];
		}
		//
		if(!empty($data['check_ecs_charges'])){
			$product->check_ecs_charges = $data['check_ecs_charges'];
		}
						
		if(!empty($data)){
			
			    Charge::where('product_id',$product->id)->delete();
				$product->save();
				
				 if(!empty($data['group-a'])){
					foreach($data['group-a'] as $key=>$value){
						 
						 $charges  = new Charge;
						 $charges->charge_name = !empty($value['charge_name'])?$value['charge_name']:null;
						 $charges->charge_type = !empty($value['charge_type'])?$value['charge_type']:null;
						 $charges->charge_amount = !empty($value['charge_amount'])?$value['charge_amount']:null;
						 $charges->charge_distrubal_amount = !empty($value['charge_distrubal_amount'])?$value['charge_distrubal_amount']:null;
						 $charges->charge_percentage = !empty($value['charge_percentage'])?$value['charge_percentage']:null;
						 $charges->user_id =  $userId;
						 $charges->product_id   =  $product->id;
						 $charges->save(); 
					 
					 }	
				 }
				 
				return redirect('manage-products')->with('status',"update successfully");
				
		}
		return view('product/edit',['productInfo'=>$product]);
        //
    }
}
