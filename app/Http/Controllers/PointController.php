<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Point;
use App\Models\CatPoint;
use Illuminate\Support\Facades\Auth;

class PointController extends Controller
{
	public function index()
    {
		$userId = Auth::check() ? Auth::id() : true;
		$pointInfo = Point::where('user_id',$userId)->get();
		
		return view('point/index',['pointInfo'=>$pointInfo]);
        
    }
    public function add(Request $request)
    {
		$data = $request->post();
		$userId = Auth::check() ? Auth::id() : true;
		$point = new Point;
		$point->user_id = $userId;
		//$point->category = !empty($data['category'])?$data['category']:0;
		if(!empty($data['category'])){
			$point->category = $data['category'];
		}		
		if(!empty($data)){
				$point->save();
				
				if(!empty($data['group-a'])){
					foreach($data['group-a'] as $key=>$value){
						$catPoint  = new CatPoint;
						$catPoint->name = !empty($value['name'])?$value['name']:null;
						$catPoint->points = !empty($value['points'])?$value['points']:null;
						$catPoint->user_id =  $userId;
						$catPoint->point_id   =  $point->id;
						$catPoint->save();
					}	
				}
				return redirect('credit-points')->with('status',"Insert successfully");
				
		}
		
		//$category->category_id = !empty($data['category_id'])?$data['category_id']:0;
		return view('point/add');
        
    }
	 public function editPoint(Request $request,$id)
    {
		
		$data = $request->post();
		$userId = Auth::check() ? Auth::id() : true;
		$pointInfo = Point::find($id);
		
		return view('point/edit',['pointInfo'=>$pointInfo]);
        //
    }
	
	public function updatePoint(Request $request)
    {
		
		$data = $request->post();
		$userId = Auth::check() ? Auth::id() : true;
		
		$pointInfo = Point::find($data['point_id']);
		
		
		if(!empty($data['category'])){
			$pointInfo->category = $data['category'];
		}		
		if(!empty($data)){
				$pointInfo->save();
				CatPoint::where('point_id',$pointInfo->id)->delete();
				if(!empty($data['group-a'])){
					foreach($data['group-a'] as $key=>$value){
						$catPoint  = new CatPoint;
						$catPoint->name = !empty($value['name'])?$value['name']:null;
						$catPoint->points = !empty($value['points'])?$value['points']:null;
						$catPoint->user_id =  $userId;
						$catPoint->point_id   =  $pointInfo->id;
						$catPoint->save();
					}	
				}
				return redirect('credit-points')->with('status',"Insert successfully");
				
		}
		if(!empty($data)){
				$categoryInfo->save();
				return redirect('credit-points')->with('status',"Insert successfully");
				
		}
        //
    }
    //
}
