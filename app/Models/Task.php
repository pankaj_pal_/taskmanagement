<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;
     /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
    	'user_id',
        'project_id',
        'name',
        'start_date',
        'end_date',
        'description',
        'status'
    ];

    /**
     * Get the comment associated with the projects tasks.
     */

    /**
     * Get the task associated with the projects tasks.
     */
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
        /**
     * Get the project that owns the tasks.
     */
    public function project()
    {
        return $this->belongsTo(Project::class);
    }
}
