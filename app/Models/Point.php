<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Point extends Model
{
    use HasFactory;
	
	public function user()
    {
        return $this->hasOne(User::class);
    }
	/**
     * Get the charge associated with the user.
     */
    public function catPoint()
    {
        return $this->hasMany(CatPoint::class,'point_id');
    }
}
