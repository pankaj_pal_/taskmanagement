<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;
     /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
    	'user_id',
        'name',
        'description',
        'status'
    ];


    /**
     * Get the task associated with the projects tasks.
     */
    public function tasks()
    {
        return $this->hasMany(Task::class, 'project_id');
    }
}
