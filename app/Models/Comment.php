<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
    	'user_id',
        'project_id',
        'task_id',
		'description',
        'status'
    ];

    /**
     * Get the comment associated with the projects tasks.
     */
    public function replies()
    {
        return $this->hasMany(ReplyComment::class, 'comment_id');
    }

}
