<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
	
	/**
     * Get the user  has associated with the user.
     */
    public function user()
    {
        return $this->hasOne(User::class);
    }
}
