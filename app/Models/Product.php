<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
	
	/**
     * Get the charge associated with the user.
     */
    public function charge()
    {
        return $this->hasMany(Charge::class,'product_id');
    }
	/**
     * Get the user  has associated with the user.
     */
    public function user()
    {
        return $this->hasOne(User::class);
    }
}
