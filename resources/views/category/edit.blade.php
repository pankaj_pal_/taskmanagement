@extends('layouts.dashboard')
      <!-- Start right Content here -->
      <!-- ============================================================== -->
      <div class="main-content">
        <div class="page-content">
          <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
              <div class="col-12">
                <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                  <h4 class="mb-sm-0 font-size-18">Add Category</h4>
                  <!-- <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                      <li class="breadcrumb-item">
                        <a href="product.html">Product</a>
                      </li>
                      <li class="breadcrumb-item active">Add Product</li>
                    </ol>
                  </div> -->
                </div>
              </div>
            </div>
            <!-- end page title -->
            <div class="row">
               <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css" integrity="sha512-xmGTNt20S0t62wHLmQec2DauG9T+owP9e6VU8GigI0anN7OXLip9i7IwEhelasml2osdxX71XcYm6BQunTQeQg==" crossorigin="anonymous" />
      <style type="text/css">
        .bootstrap-tagsinput{
            width: 100%;
        }
        .label-info{
            background-color: #5156be;

        }
        .label {
            display: inline-block;
            padding: .3em .3em;
            margin-bottom: 5px;
            font-size: .9rem;
            font-weight: 500;
            line-height: 1;
            text-align: center;
            white-space: nowrap;
            vertical-align: baseline;
            border-radius: .25rem;
            transition: color .15s ease-in-out,background-color .15s ease-in-out,
            border-color .15s ease-in-out,box-shadow .15s ease-in-out;
        }
        .bootstrap-tagsinput {
    background-color: #fff;
    border: 1px solid #ccc;
    /* box-shadow: inset 0 1px 1px rgb(0 0 0 / 8%); */
    display: inline-block;
    padding: 0.47rem 0.75rem;
    color: #555;
    vertical-align: middle;
    border-radius: 4px;
    max-width: 100%;
    line-height: 22px;
    cursor: text;
}
    </style>
			  <div class="col-lg-12">
                <div class="card">
                  <!-- <div class="card-header"><h4 class="card-title">Form layouts</h4><p class="card-title-desc">Form layout options : from inline, horizontal & custom grid implementations</p></div> -->
                  <div class="card-body p-4">
				  	<form method="POST" action="/update-category">
				
					
                        @csrf
                    <div class="row">

<div class="col-md-4 mb-3">
                                                        <label for="example-text-input" class="form-label">Select Category Name</label>
														<input type="hidden" name="category_id" value="<?php echo $categoryInfo['id'] ?>">
														<select  name="category" class="form-select">
                                                            <option <?php if($categoryInfo['category'] ==1) {
																echo "selected";
															} ?>value="1">Red</option>
                                                            <option <?php if($categoryInfo->category_id == 2){?> selected = "selected" <?php } ?> value="2">Amber</option>
                                                            <option <?php if($categoryInfo->category_id == 3){?> selected = "selected" <?php } ?>  value="3">Green</option>
                                                            
                                                        </select>
                                                    </div>
                                                    <div class="col-md-8 mb-3">
                                                        <label for="example-text-input" class="form-label">Enter Keywords (then press enter)</label>

                                                   <input type="text" data-role="tagsinput" name="keywords" value="<?php echo !empty($categoryInfo['keywords'])?$categoryInfo['keywords']:''; ?>" class="form-control">
												   </div>
                                                 
                                                  </div>
 



<div class="row">
                            <div class="col-sm-12">
                              <div class="d-flex flex-wrap gap-3">
                                
                                <button type="submit" class="btn btn-lg btn-primary w-md">Submit</button>
                              </div>
                            </div>
                          </div>
	</form>
                    
                  </div>
                </div>
              </div>
            </div>
            <!-- End Form Layout -->
          </div>
          <!-- container-fluid -->
        </div>
        <!-- End Page-content -->
        <footer class="footer">
          <div class="container-fluid">
            <div class="row">
              <div class="col-sm-6"> © <script>
                  document.write(new Date().getFullYear())
                </script> Zed Finance. </div>
              <div class="col-sm-6"></div>
            </div>
          </div>
        </footer>
      </div>
      <!-- end main content-->
    </div>
    <!-- END layout-wrapper -->
    <!-- Right Sidebar -->
    <div class="right-bar">
      <div data-simplebar class="h-100">
        <div class="rightbar-title d-flex align-items-center bg-dark p-3">
          <h5 class="m-0 me-2 text-white">Theme Customizer</h5>
          <a href="javascript:void(0);" class="right-bar-toggle ms-auto">
            <i class="mdi mdi-close noti-icon"></i>
          </a>
        </div>
        <!-- Settings -->
        <hr class="m-0" />
        <div class="p-4">
          <h6 class="mb-3">Layout</h6>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="layout" id="layout-vertical" value="vertical">
            <label class="form-check-label" for="layout-vertical">Vertical</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="layout" id="layout-horizontal" value="horizontal">
            <label class="form-check-label" for="layout-horizontal">Horizontal</label>
          </div>
          <h6 class="mt-4 mb-3 pt-2">Layout Mode</h6>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="layout-mode" id="layout-mode-light" value="light">
            <label class="form-check-label" for="layout-mode-light">Light</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="layout-mode" id="layout-mode-dark" value="dark">
            <label class="form-check-label" for="layout-mode-dark">Dark</label>
          </div>
          <h6 class="mt-4 mb-3 pt-2">Layout Width</h6>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="layout-width" id="layout-width-fuild" value="fuild" onchange="document.body.setAttribute('data-layout-size', 'fluid')">
            <label class="form-check-label" for="layout-width-fuild">Fluid</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="layout-width" id="layout-width-boxed" value="boxed" onchange="document.body.setAttribute('data-layout-size', 'boxed')">
            <label class="form-check-label" for="layout-width-boxed">Boxed</label>
          </div>
          <h6 class="mt-4 mb-3 pt-2">Layout Position</h6>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="layout-position" id="layout-position-fixed" value="fixed" onchange="document.body.setAttribute('data-layout-scrollable', 'false')">
            <label class="form-check-label" for="layout-position-fixed">Fixed</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="layout-position" id="layout-position-scrollable" value="scrollable" onchange="document.body.setAttribute('data-layout-scrollable', 'true')">
            <label class="form-check-label" for="layout-position-scrollable">Scrollable</label>
          </div>
          <h6 class="mt-4 mb-3 pt-2">Topbar Color</h6>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="topbar-color" id="topbar-color-light" value="light" onchange="document.body.setAttribute('data-topbar', 'light')">
            <label class="form-check-label" for="topbar-color-light">Light</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="topbar-color" id="topbar-color-dark" value="dark" onchange="document.body.setAttribute('data-topbar', 'dark')">
            <label class="form-check-label" for="topbar-color-dark">Dark</label>
          </div>
          <h6 class="mt-4 mb-3 pt-2 sidebar-setting">Sidebar Size</h6>
          <div class="form-check sidebar-setting">
            <input class="form-check-input" type="radio" name="sidebar-size" id="sidebar-size-default" value="default" onchange="document.body.setAttribute('data-sidebar-size', 'lg')">
            <label class="form-check-label" for="sidebar-size-default">Default</label>
          </div>
          <div class="form-check sidebar-setting">
            <input class="form-check-input" type="radio" name="sidebar-size" id="sidebar-size-compact" value="compact" onchange="document.body.setAttribute('data-sidebar-size', 'md')">
            <label class="form-check-label" for="sidebar-size-compact">Compact</label>
          </div>
          <div class="form-check sidebar-setting">
            <input class="form-check-input" type="radio" name="sidebar-size" id="sidebar-size-small" value="small" onchange="document.body.setAttribute('data-sidebar-size', 'sm')">
            <label class="form-check-label" for="sidebar-size-small">Small (Icon View)</label>
          </div>
          <h6 class="mt-4 mb-3 pt-2 sidebar-setting">Sidebar Color</h6>
          <div class="form-check sidebar-setting">
            <input class="form-check-input" type="radio" name="sidebar-color" id="sidebar-color-light" value="light" onchange="document.body.setAttribute('data-sidebar', 'light')">
            <label class="form-check-label" for="sidebar-color-light">Light</label>
          </div>
          <div class="form-check sidebar-setting">
            <input class="form-check-input" type="radio" name="sidebar-color" id="sidebar-color-dark" value="dark" onchange="document.body.setAttribute('data-sidebar', 'dark')">
            <label class="form-check-label" for="sidebar-color-dark">Dark</label>
          </div>
          <div class="form-check sidebar-setting">
            <input class="form-check-input" type="radio" name="sidebar-color" id="sidebar-color-brand" value="brand" onchange="document.body.setAttribute('data-sidebar', 'brand')">
            <label class="form-check-label" for="sidebar-color-brand">Brand</label>
          </div>
          <h6 class="mt-4 mb-3 pt-2">Direction</h6>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="layout-direction" id="layout-direction-ltr" value="ltr">
            <label class="form-check-label" for="layout-direction-ltr">LTR</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="layout-direction" id="layout-direction-rtl" value="rtl">
            <label class="form-check-label" for="layout-direction-rtl">RTL</label>
          </div>
        </div>
      </div>
      <!-- end slimscroll-menu-->
    </div>
    <!-- /Right-bar -->
    <!-- Right bar overlay-->
    <div class="rightbar-overlay"></div>
	<script src="{{ asset('libs/jquery/jquery.min.js') }}" ></script>
	<script src="{{ asset('libs/bootstrap/js/bootstrap.bundle.min.js') }}" ></script>
	<script src="{{ asset('libs/metismenu/metisMenu.min.js') }}" ></script>
	<script src="{{ asset('libs/simplebar/simplebar.min.js') }}" ></script>
	<script src="{{ asset('libs/node-waves/waves.min.js') }}" ></script>
	<script src="{{ asset('libs/feather-icons/feather.min.js') }}" ></script>
	<script src="{{ asset('libs/pace-js/pace.min.js') }}" ></script>
    <!-- pace js -->
    <script src="assets/libs/pace-js/pace.min.js"></script>
    
	<script src="{{ asset('js/app.js') }}" ></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js" integrity="sha512-VvWznBcyBJK71YKEKDMpZ0pCVxjNuKwApp4zLF3ul+CiflQi6aIJR+aZCP/qWsoFBA28avL5T5HA+RE+zrGQYg==" crossorigin="anonymous"></script>
    
    <script src="{{ asset('js/jquery.repeater.min.js') }}"></script>
    <script type="text/javascript">
      // form repeater Initialization
      $('.repeater-default').repeater({
        show: function() {
          $(this).slideDown();
        },
        hide: function(deleteElement) {
          if (confirm('Are you sure you want to delete this element?')) {
            $(this).slideUp(deleteElement);
          }
        }
      });
    </script>
	
	
	
	@section('content')

@endsection
	
	
  
