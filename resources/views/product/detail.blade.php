@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Task Details') }}</div>

                <div class="card-body">
                 <table class="table">
    <thead>
      <tr>
        <th>Name</th>
        <th>Project Name</th>
        <th>Status</th>
        <th>Start Date</th>
        <th>End Date</th>
        
        
      </tr>
    <tbody>
      <tr>
         <td><?php echo $taskList->name; ?></td>
        <td><?php echo $taskList->project->name; ?></td>
        <td><?php 
        $status = $taskList->status;
        switch ($status) {
            case 1:
                echo "To Do";
                break;
            case 2:
                echo "In Progress";
                break;
            case 3:
                echo "Done";
                break;
            default:
             echo "To Do";
        } 
                ?>
                    
                </td>
                <td>
                    <?php echo date('Y-m-d',strtotime($taskList->start_date)); ?>
                </td>
                <td>
                    <?php echo date('Y-m-d',strtotime($taskList->end_date)); ?>
                </td>
       </tr>  
    </thead>
    
    </tbody>
</table>
<form method="POST" action="/add-comment">
                        @csrf

                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Add Comment') }}</label>

                            <div class="col-md-6">
                                <input id="task" type="hidden"  name="task_id" value="<?php echo $taskList[
                                'id']; ?>">
                                <input id="task" type="hidden"  name="project_id" value="<?php echo $taskList[
                                'project_id']; ?>">
                                <input id="description" type="text" class="form-control @error('description') is-invalid @enderror" name="description" value="{{ old('description') }}" required autocomplete="description" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        

                        <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Add') }}
                                </button>
                            </div>
                        </div>
                    </form>
                    
                            <table class="table">
    <thead>
      <tr>
        <th>Comment</th>
        <th>Comment Date</th>
        <th>Reply Comment</th>
        </tr>
        <tbody>
                    <?php 
                     if(!empty($taskList->comments)){
                        foreach($taskList->comments as $comment){
                            ?>
                        <tr>
                            <td><?php echo $comment->description; ?></td>
                            <td>
                                <?php echo date("Y-m-d h:i:s a",strtotime($comment->created_at)); ?>
                                
                            </td>
                            <td>
                                <a href="/reply/{{$comment->id}}">
                                   Reply
                                </a>
                            </td>
                         </tr>   
                        <?php }

                    } ?>
                </tbody>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
