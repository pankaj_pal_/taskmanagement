@extends('layouts.addproduct')
      <!-- Start right Content here -->
      <!-- ============================================================== -->
      <div class="main-content">
        <div class="page-content">
          <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
              <div class="col-12">
                <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                  <h4 class="mb-sm-0 font-size-18">Add Product</h4>
                  <!-- <div class="page-title-right"><ol class="breadcrumb m-0"><li class="breadcrumb-item"><a href="product.html">Product</a></li><li class="breadcrumb-item active">Add Product</li></ol></div> -->
                </div>
              </div>
            </div>
            <!-- end page title -->
			<form method="POST" action="{{ route('save-product') }}">
			
                        @csrf
            <div class="row">
              <div class="col-lg-12">
                <div class="card">
                  <!-- <div class="card-header"><h4 class="card-title">Form layouts</h4><p class="card-title-desc">Form layout options : from inline, horizontal & custom grid implementations</p></div> -->
                  <div class="card-body p-4">
                    <div class="row">
                      <div class="alert alert-light fw-semibold" role="alert"> Step 1 </div>
                      <div class="col-md-4 mb-3">
                        <label for="example-text-input" class="form-label">Product Category</label>
                        <select name="category_id" class="form-select">
                          <option value="1">Credit Line</option>
                          <option value="2">Loan</option>
                        </select>
                      </div>
                      <div class="col-md-4 mb-3">
                        <label for="example-text-input" class="form-label">Product Name</label>
                        <input class="form-control" name="product_name" type="text" placeholder="enter product name" id="example-text-input">
                      </div>
                      <div class="alert alert-light fw-semibold" role="alert"> Step 2 </div>
                      <div class="col-md-4 mb-3">
                        <label for="example-text-input" class="form-label">Loan Amount Range</label>
                        <div class="row">
                          <div class="col-sm-6">
                            <input type="text"  name="loan_amount_min" class="form-control" id="horizontal-email-input" placeholder="Min Amount">
                          </div>
                          <div class="col-sm-6">
                            <input type="text" name="loan_amount_max" class="form-control" id="horizontal-email-input" placeholder="Max Amount">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4 mb-3">
                        <label for="example-text-input" class="form-label">Tenure</label>
                        <div class="row">
                          <div class="col-sm-6">
                            <input type="text" name="tenure" class="form-control" id="horizontal-email-input" placeholder="eg. 15">
                          </div>
                          <div class="col-sm-6">
                            <select name="tenure_type" class="form-select">
                              <option value="">Select Type</option>
                              <option value="1">Day</option>
                              <option value="2">Week</option>
                              <option value="3">Month</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4 mb-3">
                        <label for="example-text-input" class="form-label">Rate of Interest</label>
                        <div class="row">
                          <div class="col-sm-6">
                            <input type="text"name="rate_of_interest" class="form-control" id="horizontal-email-input" placeholder="enter %">
                          </div>
                          <div class="col-sm-6">
                            <select name="frequency_id" class="form-select">
                              <option value="">Select Frequency</option>
                              <option value="1">Per Day</option>
                              <option value="2">Weekly</option>
                              <option value="3">Monthly</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="alert alert-light fw-semibold" role="alert"> Step 3 </div>
                      <div class="col-md-3 mb-3">
                        <label for="example-text-input" class="form-label">Processing Fee Applicable?</label>

                        <select name="processing_fee" class="form-select">
                              <option value="1">Yes</option>
                              <option value="2">No</option>
                        </select>

                      </div>
                      <div class="col-md-4 mb-3">
                      <label for="example-text-input" class="form-label">Add Charges</label>
                      <div class="row">

                          <div class="col-sm-6">
                            <input type="text" name="add_charges" class="form-control" id="horizontal-email-input" placeholder="enter %">
                          </div>
                          <div class="col-sm-6">
                            <input type="text" name="amount" class="form-control" id="horizontal-email-input" placeholder="enter amount">
                          </div>
                          <div class="col-sm-12"><span class="font-size-12 text-primary">Minimum Processing fee ₹500/2% which ever is higher</span></div>

                        </div>
                      
                    </div>
                      <div class="col-md-4 mb-3">
                        <label for="example-text-input" class="form-label">GST Applicable?</label>
                        <div class="row">
                          <div class="col-sm-6">
                            <select name="gst_applicable" class="form-select">
                              <option value="1">Yes</option>
                              <option value="2">No</option>
                            </select>
                          </div>
                          <div class="col-sm-6">
                            <input type="text" name="gst_percentage_amount" class="form-control" id="horizontal-email-input" placeholder="enter gst %">
                          </div>
                        </div>
                      </div>
                      
                      
                   
                    <div class="alert alert-light fw-semibold" role="alert"> Step 4 </div>
                    <div class="form repeater-default">
                      <div data-repeater-list="group-a">
                        <div data-repeater-item>
                          <div class="row">
                            <div class="col-md-2 col-sm-12 form-group">
                              <label for="Email">Charge Name</label>
                              <input type="email" name="charge_name" class="form-control" id="Email" placeholder="Enter email id">
                            </div>
                            <div class="col-md-2 col-sm-12 form-group">
                              <label for="gender">Type of charge</label>
                              <select  name="charge_type" id="gender" class="form-select">
                                <option value="1">One Time</option>
                                <option value="2">Recurring</option>
                              </select>
                            </div>
                            <div class="col-md-2 col-sm-12 form-group">
                              <label for="Profession">Select</label>
                              <select name="charge_amount"  id="Profession" class="form-select">
                                <option value="1">%</option>
                                <option value="2">Fixed Amount</option>
                              </select>
                            </div>
                            <div class="col-md-2 col-sm-12 form-group">
                              <label for="Profession">Select</label>
                              <select name="charge_distrubal_amount"  id="Profession" class="form-select">
                                <option value="1">Disbursal Amount</option>
                                <option value="2">Approved Amount</option>
                              </select>
                            </div>
                            <div class="col-md-2 col-sm-12 form-group">
                              <label for="Email">Enter %</label>
                              <input name="charge_percentage"  type="text"  class="form-control" id="Email" placeholder="Enter %">
                            </div>
                            <div class="col-md-2 col-sm-12 form-group d-flex align-items-center pt-4">
                              <button class="btn btn-danger" data-repeater-delete type="button">
                                <i class="bx bx-x"></i> Delete </button>
                            </div>
                          </div>
                          <hr>
                        </div>
                      </div>
                      <div class="form-group mb-3">
                        <div class="col p-0">
                          <button class="btn btn-outline-secondary waves-effect" data-repeater-create type="button">
                            <i class="bx bx-plus"></i> Add Other Charges </button>
                        </div>
                      </div>
                    </div>
                    <div class="alert alert-light fw-semibold" role="alert"> Step 5 </div>
                    
                      <div class="col-md-4 mb-3">
                        <label for="example-text-input"  class="form-label">Penal Interest</label>
                        <div class="row">
                          <div class="col-sm-6">
                            <input type="text" name="penal_intrest" class="form-control" id="horizontal-email-input" placeholder="enter %">
                          </div>
                          <div class="col-sm-6">
                            <select name="penal_amount" class="form-select">
                              <option value="1">% of which Amount</option>
                              <option value="2">Disbursal Amount</option>
                              <option value="3">Approved Amount</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6 mb-3">
                        <label for="example-text-input" class="form-label">Cheque/ECS Returns Charges Applicable?</label>
                        <div class="row">
                          <div class="col-sm-4">
                            <select name="check_ecs_charges" class="form-select">
                              <option value="1">Yes</option>
                              <option value="2">No</option>
                            </select>
                          </div>
                          <div class="col-sm-4">
                            <input type="text" name="gst_percentage" class="form-control" id="horizontal-email-input" placeholder="enter gst %">
                          </div>
                          <div class="col-sm-4">
                            <select name="approve_amount" class="form-select">
                              <option value="1">% of which Amount</option>
                              <option value="2">Disbursal Amount</option>
                              <option value="3">Approved Amount</option>
                            </select>
                          </div>
                        </div>
                      </div>

                      </div>
                   
                    <div class="row mt-3">
                      <div class="col-sm-12">
                        <div class="d-flex flex-wrap gap-3">
                          <button type="submit" class="btn btn-primary w-md">Submit</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
			</form>
            <!-- End Form Layout -->
          </div>
          <!-- container-fluid -->
        </div>
        <!-- End Page-content -->
        <footer class="footer">
          <div class="container-fluid">
            <div class="row">
              <div class="col-sm-6"> © <script>
                  document.write(new Date().getFullYear())
                </script> Zed Finance. </div>
              <div class="col-sm-6"></div>
            </div>
          </div>
        </footer>
      </div>
      <!-- end main content-->
    </div>
    <!-- END layout-wrapper -->
    <!-- Right Sidebar -->
    <div class="right-bar">
      <div data-simplebar class="h-100">
        <div class="rightbar-title d-flex align-items-center bg-dark p-3">
          <h5 class="m-0 me-2 text-white">Theme Customizer</h5>
          <a href="javascript:void(0);" class="right-bar-toggle ms-auto">
            <i class="mdi mdi-close noti-icon"></i>
          </a>
        </div>
        <!-- Settings -->
        <hr class="m-0" />
        <div class="p-4">
          <h6 class="mb-3">Layout</h6>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="layout" id="layout-vertical" value="vertical">
            <label class="form-check-label" for="layout-vertical">Vertical</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="layout" id="layout-horizontal" value="horizontal">
            <label class="form-check-label" for="layout-horizontal">Horizontal</label>
          </div>
          <h6 class="mt-4 mb-3 pt-2">Layout Mode</h6>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="layout-mode" id="layout-mode-light" value="light">
            <label class="form-check-label" for="layout-mode-light">Light</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="layout-mode" id="layout-mode-dark" value="dark">
            <label class="form-check-label" for="layout-mode-dark">Dark</label>
          </div>
          <h6 class="mt-4 mb-3 pt-2">Layout Width</h6>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="layout-width" id="layout-width-fuild" value="fuild" onchange="document.body.setAttribute('data-layout-size', 'fluid')">
            <label class="form-check-label" for="layout-width-fuild">Fluid</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="layout-width" id="layout-width-boxed" value="boxed" onchange="document.body.setAttribute('data-layout-size', 'boxed')">
            <label class="form-check-label" for="layout-width-boxed">Boxed</label>
          </div>
          <h6 class="mt-4 mb-3 pt-2">Layout Position</h6>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="layout-position" id="layout-position-fixed" value="fixed" onchange="document.body.setAttribute('data-layout-scrollable', 'false')">
            <label class="form-check-label" for="layout-position-fixed">Fixed</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="layout-position" id="layout-position-scrollable" value="scrollable" onchange="document.body.setAttribute('data-layout-scrollable', 'true')">
            <label class="form-check-label" for="layout-position-scrollable">Scrollable</label>
          </div>
          <h6 class="mt-4 mb-3 pt-2">Topbar Color</h6>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="topbar-color" id="topbar-color-light" value="light" onchange="document.body.setAttribute('data-topbar', 'light')">
            <label class="form-check-label" for="topbar-color-light">Light</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="topbar-color" id="topbar-color-dark" value="dark" onchange="document.body.setAttribute('data-topbar', 'dark')">
            <label class="form-check-label" for="topbar-color-dark">Dark</label>
          </div>
          <h6 class="mt-4 mb-3 pt-2 sidebar-setting">Sidebar Size</h6>
          <div class="form-check sidebar-setting">
            <input class="form-check-input" type="radio" name="sidebar-size" id="sidebar-size-default" value="default" onchange="document.body.setAttribute('data-sidebar-size', 'lg')">
            <label class="form-check-label" for="sidebar-size-default">Default</label>
          </div>
          <div class="form-check sidebar-setting">
            <input class="form-check-input" type="radio" name="sidebar-size" id="sidebar-size-compact" value="compact" onchange="document.body.setAttribute('data-sidebar-size', 'md')">
            <label class="form-check-label" for="sidebar-size-compact">Compact</label>
          </div>
          <div class="form-check sidebar-setting">
            <input class="form-check-input" type="radio" name="sidebar-size" id="sidebar-size-small" value="small" onchange="document.body.setAttribute('data-sidebar-size', 'sm')">
            <label class="form-check-label" for="sidebar-size-small">Small (Icon View)</label>
          </div>
          <h6 class="mt-4 mb-3 pt-2 sidebar-setting">Sidebar Color</h6>
          <div class="form-check sidebar-setting">
            <input class="form-check-input" type="radio" name="sidebar-color" id="sidebar-color-light" value="light" onchange="document.body.setAttribute('data-sidebar', 'light')">
            <label class="form-check-label" for="sidebar-color-light">Light</label>
          </div>
          <div class="form-check sidebar-setting">
            <input class="form-check-input" type="radio" name="sidebar-color" id="sidebar-color-dark" value="dark" onchange="document.body.setAttribute('data-sidebar', 'dark')">
            <label class="form-check-label" for="sidebar-color-dark">Dark</label>
          </div>
          <div class="form-check sidebar-setting">
            <input class="form-check-input" type="radio" name="sidebar-color" id="sidebar-color-brand" value="brand" onchange="document.body.setAttribute('data-sidebar', 'brand')">
            <label class="form-check-label" for="sidebar-color-brand">Brand</label>
          </div>
          <h6 class="mt-4 mb-3 pt-2">Direction</h6>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="layout-direction" id="layout-direction-ltr" value="ltr">
            <label class="form-check-label" for="layout-direction-ltr">LTR</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="layout-direction" id="layout-direction-rtl" value="rtl">
            <label class="form-check-label" for="layout-direction-rtl">RTL</label>
          </div>
        </div>
      </div>
      <!-- end slimscroll-menu-->
    </div>
    <!-- /Right-bar -->
    <!-- Right bar overlay-->
    <div class="rightbar-overlay"></div>
    <!-- JAVASCRIPT -->
    
	<script src="{{ asset('libs/jquery/jquery.min.js') }}" ></script>
	<script src="{{ asset('libs/bootstrap/js/bootstrap.bundle.min.js') }}" ></script>
	<script src="{{ asset('libs/metismenu/metisMenu.min.js') }}" ></script>
	<script src="{{ asset('libs/simplebar/simplebar.min.js') }}" ></script>
	<script src="{{ asset('libs/node-waves/waves.min.js') }}" ></script>
	<script src="{{ asset('libs/feather-icons/feather.min.js') }}" ></script>
	<script src="{{ asset('libs/pace-js/pace.min.js') }}" ></script>
    <!-- pace js -->
    <script src="assets/libs/pace-js/pace.min.js"></script>
    
	<script src="{{ asset('js/app.js') }}" ></script>
    <script src="{{ asset('js/jquery.repeater.min.js') }}"></script>
    <script type="text/javascript">
      // form repeater Initialization
      $('.repeater-default').repeater({
        show: function() {
          $(this).slideDown();
        },
        hide: function(deleteElement) {
          if (confirm('Are you sure you want to delete this element?')) {
            $(this).slideUp(deleteElement);
          }
        }
      });
    </script>
	
	
	@section('content')

@endsection
	
	
  </body>
</html>
