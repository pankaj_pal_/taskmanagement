@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Edit Task') }}</div>

                <div class="card-body">
                    <form method="POST" action="/update-task">
                        @csrf

                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input type="hidden" name="task_id" value="<?php echo $taskList['id']; ?>">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="<?php echo $taskList['name'] ?>" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="row mb-3">
                            <label for="start_date" class="col-md-4 col-form-label text-md-end">{{ __('Start Date') }}</label>

                            <div class="col-md-6">
                                <input id="start_date" type="date" class="form-control @error('start_date') is-invalid @enderror" name="start_date" value="<?php 
                                    echo date('Y-m-d',strtotime($taskList['start_date']));
                                ?>" required autocomplete="name" autofocus>

                                @error('start_date')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="end_date" class="col-md-4 col-form-label text-md-end">{{ __('End Date') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="date" class="form-control @error('end_date') is-invalid @enderror" name="end_date" value="<?php 
                                    echo date('Y-m-d',strtotime($taskList['end_date']));
                                ?>" required autocomplete="end_date" autofocus>

                                @error('end_date')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="Select Project" class="col-md-4 col-form-label text-md-end">{{ __('Select Project') }}</label>
                            <div class="col-md-6">
                                <select name="project_id" class="form-control @error('project_id') is-invalid @enderror" required autocomplete="project_id" autofocus>
                                    <option value="">Select Project</option>
                                    <?php

                                     foreach($projectInfo as $value){

                                        $selected = ($value['id'] == $taskList['project_id'])?'selected=TRUE':false;
                                       
                                        ?>
                                    <option <?php echo $selected;?> value="<?php echo $value['id']; ?>">
                                        <?php echo $value['name']; ?>
                                    </option>
                                    
                                <?php } ?>
                                </select>
                                @error('project_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="Select Project" class="col-md-4 col-form-label text-md-end">{{ __('Task Status') }}</label>

                            <div class="col-md-6">
                                <select name="status" class="form-control @error('project_id') is-invalid @enderror" required autocomplete="project_id" autofocus>
                                    <option value="">Select Status</option>
                                   <?php 
                                    $array_status = array('1'=>'To Do','2'=>'In Progress','3'=>'Done');
                                    foreach($array_status as $key=>$value){
                                        $selected_status = ($key == $taskList['status'])?'selected=TRUE':false;
                                        ?>
                                        <option <?php echo $selected_status; ?> value="<?php echo $key; ?>">
                                            <?php echo $value; ?>
                                        </option>
                                <?php } ?>
                                </select>
                                @error('status')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="row mb-3">
                            <label for="description" class="col-md-4 col-form-label text-md-end">{{ __('Description') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="text" class="form-control" name="description" value="<?php echo $taskList['description']; ?>"  autocomplete="description">

                                @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Update') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
