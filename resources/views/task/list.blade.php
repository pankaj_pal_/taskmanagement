@extends('layouts.app')

@section('content')
<div class="container">
     @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Manage Task') }}

                </div>

                <div class="card-body">
                    
                                <table class="table">
    <thead>
      <tr>
        <th>Name</th>
        <th>Project Name</th>
        <th>Status</th>
        <th>View Details</th>
        <th>Action</th>
        
      </tr>
    </thead>
    <tbody>
            <?php 
                foreach($taskList as $taskDetails){
                    ?>
                    <tr>
        <td><?php echo $taskDetails->name; ?></td>
        <td><?php echo $taskDetails->project->name; ?></td>
        <td><?php 
        $status = $taskDetails->status;
        switch ($status) {
            case 1:
                echo "To Do";
                break;
            case 2:
                echo "In Progress";
                break;
            case 3:
                echo "Done";
                break;
        default:
            echo "To Do";
} 
       // echo ($taskDetails->status==1)?;

         ?></td>
        <td><a href="/detail/{{$taskDetails->id}}"><?php echo $taskDetails->name; ?></a></td>
        <td><a href="/edit/{{$taskDetails->id}}">Edit</a>/
        <a href="/delete/{{$taskDetails->id}}">Delete</td>
        <?php } ?>
      </tr>
                      
        
      
      
    </tbody>
  </table>         
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
