@extends('layouts.dashboard')
      <!-- ============================================================== -->
      <div class="main-content">
        <div class="page-content">
          <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
              <div class="col-12">
                <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                  <h4 class="mb-sm-0 font-size-18">Add Points</h4>
                  <!-- <div class="page-title-right"><ol class="breadcrumb m-0"><li class="breadcrumb-item"><a href="product.html">Product</a></li><li class="breadcrumb-item active">Add Product</li></ol></div> -->
                </div>
              </div>
            </div>
            <!-- end page title -->
            <div class="row">
			<form method="POST" action="{{ route('save-point') }}">
			
                        @csrf
              <div class="col-lg-12">
                <div class="card">
                  <!-- <div class="card-header"><h4 class="card-title">Form layouts</h4><p class="card-title-desc">Form layout options : from inline, horizontal & custom grid implementations</p></div> -->
                  <div class="card-body p-4">
                    <div class="row">
                      <div class="col-md-7 mb-3">
                        <label for="example-text-input" class="form-label">Select Category</label>
                        <select name ="category" class="form-select">
                          <option value="Personal Data">Personal Data</option>
                          <option value="Financial Data">Financial Data</option>
                          <option value="Data">Data</option>
                        </select>
                      </div>
                    </div>
					<div class="form repeater-default">
                      <div data-repeater-list="group-a">
                        <div data-repeater-item>
                          <div class="row">
                            <div class="col-md-3 col-sm-12 form-group">
                              <label for="Email">Name</label>
                              <input name="name" type="text" class="form-control" placeholder="Enter Name">
                            </div>
                            <div class="col-md-3 col-sm-12 form-group">
                              <label for="gender">Points</label>
                             <input name="points" type="number" class="form-control" placeholder="Enter Points">
                            </div>
                            <div class="col-md-2 col-sm-12 form-group d-flex align-items-center pt-4">
                              <button class="btn btn-danger" data-repeater-delete type="button">
                                <i class="bx bx-x"></i> Delete </button>
                            </div>

                            </div>
                         <div class="col-md-7">
                             <hr>
                           </div>
                          <hr>
                        </div>
                      </div>
                      <div class="form-group mb-3">
                        <div class="col p-0">
                          <button class="btn btn-outline-secondary waves-effect" data-repeater-create type="button">
                            <i class="bx bx-plus"></i> Add More </button>
                        </div>
                      </div>
                    </div>
                    
                   

                    <div class="row">
                      <div class="col-sm-12">
                        <div class="d-flex flex-wrap gap-3">
                          <button type="submit" class="btn btn-primary w-md">Submit</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
			  </form>
            </div>
            <!-- End Form Layout -->
          </div>
          <!-- container-fluid -->
        </div>
        <!-- End Page-content -->
        <footer class="footer">
          <div class="container-fluid">
            <div class="row">
              <div class="col-sm-6"> © <script>
                  document.write(new Date().getFullYear())
                </script> Zed Finance. </div>
              <div class="col-sm-6"></div>
            </div>
          </div>
        </footer>
      </div>
      <!-- end main content-->
    </div>
    <!-- END layout-wrapper -->
    <!-- Right Sidebar -->
    <div class="right-bar">
      <div data-simplebar class="h-100">
        <div class="rightbar-title d-flex align-items-center bg-dark p-3">
          <h5 class="m-0 me-2 text-white">Theme Customizer</h5>
          <a href="javascript:void(0);" class="right-bar-toggle ms-auto">
            <i class="mdi mdi-close noti-icon"></i>
          </a>
        </div>
        <!-- Settings -->
        <hr class="m-0" />
        <div class="p-4">
          <h6 class="mb-3">Layout</h6>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="layout" id="layout-vertical" value="vertical">
            <label class="form-check-label" for="layout-vertical">Vertical</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="layout" id="layout-horizontal" value="horizontal">
            <label class="form-check-label" for="layout-horizontal">Horizontal</label>
          </div>
          <h6 class="mt-4 mb-3 pt-2">Layout Mode</h6>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="layout-mode" id="layout-mode-light" value="light">
            <label class="form-check-label" for="layout-mode-light">Light</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="layout-mode" id="layout-mode-dark" value="dark">
            <label class="form-check-label" for="layout-mode-dark">Dark</label>
          </div>
          <h6 class="mt-4 mb-3 pt-2">Layout Width</h6>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="layout-width" id="layout-width-fuild" value="fuild" onchange="document.body.setAttribute('data-layout-size', 'fluid')">
            <label class="form-check-label" for="layout-width-fuild">Fluid</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="layout-width" id="layout-width-boxed" value="boxed" onchange="document.body.setAttribute('data-layout-size', 'boxed')">
            <label class="form-check-label" for="layout-width-boxed">Boxed</label>
          </div>
          <h6 class="mt-4 mb-3 pt-2">Layout Position</h6>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="layout-position" id="layout-position-fixed" value="fixed" onchange="document.body.setAttribute('data-layout-scrollable', 'false')">
            <label class="form-check-label" for="layout-position-fixed">Fixed</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="layout-position" id="layout-position-scrollable" value="scrollable" onchange="document.body.setAttribute('data-layout-scrollable', 'true')">
            <label class="form-check-label" for="layout-position-scrollable">Scrollable</label>
          </div>
          <h6 class="mt-4 mb-3 pt-2">Topbar Color</h6>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="topbar-color" id="topbar-color-light" value="light" onchange="document.body.setAttribute('data-topbar', 'light')">
            <label class="form-check-label" for="topbar-color-light">Light</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="topbar-color" id="topbar-color-dark" value="dark" onchange="document.body.setAttribute('data-topbar', 'dark')">
            <label class="form-check-label" for="topbar-color-dark">Dark</label>
          </div>
          <h6 class="mt-4 mb-3 pt-2 sidebar-setting">Sidebar Size</h6>
          <div class="form-check sidebar-setting">
            <input class="form-check-input" type="radio" name="sidebar-size" id="sidebar-size-default" value="default" onchange="document.body.setAttribute('data-sidebar-size', 'lg')">
            <label class="form-check-label" for="sidebar-size-default">Default</label>
          </div>
          <div class="form-check sidebar-setting">
            <input class="form-check-input" type="radio" name="sidebar-size" id="sidebar-size-compact" value="compact" onchange="document.body.setAttribute('data-sidebar-size', 'md')">
            <label class="form-check-label" for="sidebar-size-compact">Compact</label>
          </div>
          <div class="form-check sidebar-setting">
            <input class="form-check-input" type="radio" name="sidebar-size" id="sidebar-size-small" value="small" onchange="document.body.setAttribute('data-sidebar-size', 'sm')">
            <label class="form-check-label" for="sidebar-size-small">Small (Icon View)</label>
          </div>
          <h6 class="mt-4 mb-3 pt-2 sidebar-setting">Sidebar Color</h6>
          <div class="form-check sidebar-setting">
            <input class="form-check-input" type="radio" name="sidebar-color" id="sidebar-color-light" value="light" onchange="document.body.setAttribute('data-sidebar', 'light')">
            <label class="form-check-label" for="sidebar-color-light">Light</label>
          </div>
          <div class="form-check sidebar-setting">
            <input class="form-check-input" type="radio" name="sidebar-color" id="sidebar-color-dark" value="dark" onchange="document.body.setAttribute('data-sidebar', 'dark')">
            <label class="form-check-label" for="sidebar-color-dark">Dark</label>
          </div>
          <div class="form-check sidebar-setting">
            <input class="form-check-input" type="radio" name="sidebar-color" id="sidebar-color-brand" value="brand" onchange="document.body.setAttribute('data-sidebar', 'brand')">
            <label class="form-check-label" for="sidebar-color-brand">Brand</label>
          </div>
          <h6 class="mt-4 mb-3 pt-2">Direction</h6>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="layout-direction" id="layout-direction-ltr" value="ltr">
            <label class="form-check-label" for="layout-direction-ltr">LTR</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="layout-direction" id="layout-direction-rtl" value="rtl">
            <label class="form-check-label" for="layout-direction-rtl">RTL</label>
          </div>
        </div>
      </div>
      <!-- end slimscroll-menu-->
    </div>
    <!-- /Right-bar -->
    <!-- Right bar overlay-->
    <div class="rightbar-overlay"></div>
	
	
	
	
	@section('content')

@endsection
	
	
  
