@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reply Comment') }}</div>

                <div class="card-body">

<form method="POST" action="/reply-comment">
                        @csrf
                        
                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Reply Comment') }}</label>

                            <div class="col-md-6">
                                <input id="task" type="hidden"  name="task_id" value="<?php echo $commentList['task_id']; ?>">
                                <input id="task" type="hidden"  name="comment_id" value="<?php echo $commentList['id']; ?>">
                                <input id="task" type="hidden"  name="project_id" value="<?php echo $commentList['project_id']; ?>">
                                <input id="description" type="text" class="form-control @error('description') is-invalid @enderror" name="description" value="{{ old('description') }}" required autocomplete="description" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        

                        <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Add') }}
                                </button>
                            </div>
                        </div>
                    </form>

                    
                            </div>
            </div>
        </div>
    </div>
</div>
@endsection
