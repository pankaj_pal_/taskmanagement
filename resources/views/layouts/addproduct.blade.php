<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>Add Product</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="assets/images/favicon.ico">
		<link  href="{{ asset('css/preloader.min.css') }}" rel="stylesheet">
         <link  href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
         <link  href="{{ asset('css/icons.min.css') }}" rel="stylesheet">
         <link  href="{{ asset('css/app.min.css') }}" id="app-style" rel="stylesheet"  rel="stylesheet">
         
	
  </head>
  <body>
    <!-- <body data-layout="horizontal"> -->
    <!-- Begin page -->
    <div id="layout-wrapper">
      <header id="page-topbar">
        <div class="navbar-header">
          <div class="d-flex">
            <!-- LOGO -->
            <div class="navbar-brand-box">
              <a href="index.html" class="logo logo-dark">
                <span class="logo-sm">
                  <img src="assets/images/logo-sm.svg" alt="" height="24">
                </span>
                <span class="logo-lg">
                  <img src="assets/images/logo-sm.svg" alt="" height="24">
                  <span class="logo-txt">Zed Finance</span>
                </span>
              </a>
              <a href="index.html" class="logo logo-light">
                <span class="logo-sm">
                  <img src="assets/images/logo-sm.svg" alt="" height="24">
                </span>
                <span class="logo-lg">
                  <img src="assets/images/logo-sm.svg" alt="" height="24">
                  <span class="logo-txt">Zed Finance</span>
                </span>
              </a>
            </div>
            <button type="button" class="btn btn-sm px-3 font-size-16 header-item" id="vertical-menu-btn">
              <i class="fa fa-fw fa-bars"></i>
            </button>
            <!-- App Search-->
            <!--  <form class="app-search d-none d-lg-block"><div class="position-relative"><input type="text" class="form-control" placeholder="Search..."><button class="btn btn-primary" type="button"><i class="bx bx-search-alt align-middle"></i></button></div></form> -->
          </div>
          <div class="d-flex">
            <div class="dropdown d-inline-block d-lg-none ms-2">
              <button type="button" class="btn header-item" id="page-header-search-dropdown" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i data-feather="search" class="icon-lg"></i>
              </button>
              <div class="dropdown-menu dropdown-menu-lg dropdown-menu-end p-0" aria-labelledby="page-header-search-dropdown">
                <form class="p-3">
                  <div class="form-group m-0">
                    <div class="input-group">
                      <input type="text" class="form-control" placeholder="Search ..." aria-label="Search Result">
                      <button class="btn btn-primary" type="submit">
                        <i class="mdi mdi-magnify"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <div class="dropdown d-inline-block">
              <button type="button" class="btn header-item noti-icon position-relative" id="page-header-notifications-dropdown" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i data-feather="bell" class="icon-lg"></i>
                <span class="badge bg-danger rounded-pill">5</span>
              </button>
              <div class="dropdown-menu dropdown-menu-lg dropdown-menu-end p-0" aria-labelledby="page-header-notifications-dropdown">
                <div class="p-3">
                  <div class="row align-items-center">
                    <div class="col">
                      <h6 class="m-0"> Notifications </h6>
                    </div>
                    <div class="col-auto">
                      <a href="#!" class="small text-reset text-decoration-underline"> Unread (3)</a>
                    </div>
                  </div>
                </div>
                <div data-simplebar style="max-height: 230px;">
                  <a href="#!" class="text-reset notification-item">
                    <div class="d-flex">
                      <div class="flex-shrink-0 me-3">
                        <img src="assets/images/users/avatar-3.jpg" class="rounded-circle avatar-sm" alt="user-pic">
                      </div>
                      <div class="flex-grow-1">
                        <h6 class="mb-1">Lorem Ipsum?</h6>
                        <div class="font-size-13 text-muted">
                          <p class="mb-1">Lorem Ipsum is simply dummy text.</p>
                          <p class="mb-0">
                            <i class="mdi mdi-clock-outline"></i>
                            <span>1 hour ago</span>
                          </p>
                        </div>
                      </div>
                    </div>
                  </a>
                  <a href="#!" class="text-reset notification-item">
                    <div class="d-flex">
                      <div class="flex-shrink-0 avatar-sm me-3">
                        <span class="avatar-title bg-primary rounded-circle font-size-16">
                          <i class="bx bx-cart"></i>
                        </span>
                      </div>
                      <div class="flex-grow-1">
                        <h6 class="mb-1">Lorem Ipsum?</h6>
                        <div class="font-size-13 text-muted">
                          <p class="mb-1">Lorem Ipsum is simply dummy text</p>
                          <p class="mb-0">
                            <i class="mdi mdi-clock-outline"></i>
                            <span>3 min ago</span>
                          </p>
                        </div>
                      </div>
                    </div>
                  </a>
                  <a href="#!" class="text-reset notification-item">
                    <div class="d-flex">
                      <div class="flex-shrink-0 avatar-sm me-3">
                        <span class="avatar-title bg-success rounded-circle font-size-16">
                          <i class="bx bx-badge-check"></i>
                        </span>
                      </div>
                      <div class="flex-grow-1">
                        <h6 class="mb-1">Lorem Ipsum?</h6>
                        <div class="font-size-13 text-muted">
                          <p class="mb-1">Lorem Ipsum is simply dummy text</p>
                          <p class="mb-0">
                            <i class="mdi mdi-clock-outline"></i>
                            <span>3 min ago</span>
                          </p>
                        </div>
                      </div>
                    </div>
                  </a>
                  <a href="#!" class="text-reset notification-item">
                    <div class="d-flex">
                      <div class="flex-shrink-0 me-3">
                        <img src="assets/images/users/avatar-6.jpg" class="rounded-circle avatar-sm" alt="user-pic">
                      </div>
                      <div class="flex-grow-1">
                        <h6 class="mb-1">Lorem Ipsum?</h6>
                        <div class="font-size-13 text-muted">
                          <p class="mb-1">Lorem Ipsum is simply dummy text</p>
                          <p class="mb-0">
                            <i class="mdi mdi-clock-outline"></i>
                            <span>1 hour ago</span>
                          </p>
                        </div>
                      </div>
                    </div>
                  </a>
                </div>
                <div class="p-2 border-top d-grid">
                  <a class="btn btn-sm btn-link font-size-14 text-center" href="javascript:void(0)">
                    <i class="mdi mdi-arrow-right-circle me-1"></i>
                    <span>View More..</span>
                  </a>
                </div>
              </div>
            </div>
            <div class="dropdown d-inline-block">
              <button type="button" class="btn header-item bg-soft-light border-start border-end" id="page-header-user-dropdown" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <img class="rounded-circle header-profile-user" src="assets/images/users/avatar-1.jpg" alt="Header Avatar">
                <span class="d-none d-xl-inline-block ms-1 fw-medium">Verification Team</span>
                <i class="mdi mdi-chevron-down d-none d-xl-inline-block"></i>
              </button>
              <div class="dropdown-menu dropdown-menu-end">
                <!-- item-->
                <a class="dropdown-item" href="auth-lock-screen.html">
                  <i class="mdi mdi-lock font-size-16 align-middle me-1"></i> Lock Screen </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="auth-logout.html">
                  <i class="mdi mdi-logout font-size-16 align-middle me-1"></i> Logout </a>
              </div>
            </div>
          </div>
        </div>
      </header>
      <!-- ========== Left Sidebar Start ========== -->
      <div class="vertical-menu">
        <div data-simplebar class="h-100">
          <!--- Sidemenu -->
          <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
              <li class="menu-title" data-key="t-menu">Menu</li>
              <li>
                <a href="super-admin-index.html">
                  <i data-feather="home"></i>
                  <span data-key="t-dashboard">Dashboard</span>
                </a>
              </li>
              <li>
                <a href="javascript: void(0);" class="has-arrow">
                  <i class="mdi mdi-file-document-edit"></i>
                  <span data-key="t-authentication">Master</span>
                </a>
                <ul class="sub-menu" aria-expanded="false">
				<li><a href="{{ route('product') }}" data-key="t-login">Product</a></li>
				<li>
                    <a href="sms-categorization.html" data-key="t-login">SMS Categorization</a>
                  </li>
                  <li>
                    <a href="credit-points.html" data-key="t-login">Credit Points</a>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
          <!-- Sidebar -->
        </div>
      </div>
      <!-- Left Sidebar End -->
      <!-- ============================================================== -->
