<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
			$table->integer('gst_applicable')->nullable();
            $table->integer('category_id')->nullable();
            $table->string('product_name','255')->nullable();
            $table->string('loan_amount_min','255')->nullable();
            $table->string('loan_amount_max','255')->nullable();
            $table->string('tenure','255')->nullable();
            $table->string('rate_of_interest','255')->nullable();
            $table->integer('tenure_type')->nullable();
            $table->integer('frequency_id')->nullable();
            $table->string('processing_fee','55')->nullable();
			$table->string('add_charges','255')->nullable();
            $table->string('amount','255')->nullable();
            $table->string('penal_intrest','255')->nullable();
            $table->string('penal_amount','255')->nullable();
            $table->string('check_ecs_charges','55')->nullable();
            $table->string('gst_percentage','55')->nullable();
			$table->string('gst_percentage_amount','55')->nullable();
            $table->string('approve_amount','55')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
