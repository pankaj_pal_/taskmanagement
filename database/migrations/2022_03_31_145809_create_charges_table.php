<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('charges', function (Blueprint $table) {
            $table->id();
            $table->integer('product_id');
			$table->string('charge_name','255')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('charge_type')->nullable();
            $table->integer('charge_amount')->nullable();
            $table->integer('charge_distrubal_amount')->nullable();
            $table->string('charge_percentage','255')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('charges');
    }
}
